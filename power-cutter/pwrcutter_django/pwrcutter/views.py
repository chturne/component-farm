from rest_framework import status
from rest_framework.decorators import api_view, parser_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser

from pwrcutter.serializers import PDUSerializer, OutletActionSerializer
from pwrcutter.models import PDU


@api_view(['GET', 'POST'])
def status_pdus(request, format=None):
    """List all PDU outlet status, or create a new PDU"""
    if request.method == 'GET':
        pdus = PDU.objects.all()
        serializer = PDUSerializer(pdus, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = PDUSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@parser_classes([JSONParser])
def pdu_action(request, hostname):
    """
    Retrieve port statuses, change outlet names, etc
    """
    try:
        pdu = PDU.objects.get(hostname=hostname)
    except PDU.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'POST':
        serializer = OutletActionSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        payload = serializer.data
        pdu.outlet_set(payload["outlet"], payload["action"])

        return Response(serializer.data, status=status.HTTP_201_CREATED)
