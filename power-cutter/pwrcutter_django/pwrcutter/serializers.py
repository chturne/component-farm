from pwrcutter.models import PDU, OutletAction
from rest_framework import serializers


class PDUSerializer(serializers.ModelSerializer):
    class Meta:
        model = PDU
        fields = ['hostname']


class OutletActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutletAction
        fields = ['action', 'outlet']
