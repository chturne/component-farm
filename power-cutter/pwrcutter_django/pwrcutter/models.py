from django.db import models
from rest_framework import serializers
from easysnmp import snmp_get, snmp_set, snmp_walk

ACTION_NAMES = ['CANCEL', 'ON', 'OFF', 'REBOOT']
ACTION_IDS = ['0', '1', '2', '3']


def valid_action_name(action_name):
    return action_name.upper() in ACTION_NAMES
    

def action_name_to_id(action_name):
    return dict(zip(ACTION_NAMES, ACTION_IDS))[action_name.upper()]


def action_id_to_name(action_id):
    return dict(zip(ACTION_IDS, ACTION_NAMES))[str(action_id)]


# Create your models here.
class PDU(models.Model):
    SNMP_INTEGER_TYPE = 'i'
    OID_OUTLETS = "SNMPv2-SMI::enterprises.3808.1.1.3.3.3.1.1"

    hostname = models.TextField(primary_key=True)

    def name_to_outlet_map(self):
        """Return a mapping of labels to outlet numbers."""
        names = [x.value for x in snmp_walk(f'{PDU.OID_OUTLETS}.2',
                                            hostname=self.hostname,
                                            community='public',
                                            version=1)]
        return dict(zip(names, range(1, len(names))))

    def outlet_port_for_name(self, name):
        mapping = self.name_to_outlet_map()
        if name not in mapping:
            raise serializers.ValidationError('output label does not exist')
        return mapping[name]

    def outlet_set(self, outlet_name, action):
        if not valid_action_name(action):
            raise serializers.ValdiationError('action not valid')
        outlet_port = self.outlet_port_for_name(outlet_name)
        vs = snmp_get(f'{PDU.OID_OUTLETS}.4.{outlet_port}',
                      hostname=self.hostname,
                      version=1,
                      community='private')
        print(f"outlet {outlet_port} transition {action_id_to_name(vs.value)} -> {action.upper()}")
        if not snmp_set(f"{PDU.OID_OUTLETS}.4.{outlet_port}",
                        action_name_to_id(action),
                        PDU.SNMP_INTEGER_TYPE,
                        hostname=self.hostname,
                        version=1,
                        community='private'):
            raise Exception('failed to change status of {outlet_name}')


class OutletAction(models.Model):
    # This is only used for validation
    OUTLET_CHOICES = zip(map(lambda x: x.lower(), ACTION_NAMES), ACTION_IDS)

    action = models.CharField(choices=OUTLET_CHOICES, max_length=10)
    outlet = models.TextField()
