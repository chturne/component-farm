from django.test import TestCase
from pwrcutter.models import PDU


class AnimalTestCase(TestCase):
    def setUp(self):
        PDU.objects.create(hostname="farm-pdu-1")
        PDU.objects.create(hostname="farm-pdu-2")

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        one = PDU.objects.get(hostname="farm-pdu-1")
        two = PDU.objects.get(hostname="farm-pdu-2")
        # Mock the SNMP stuff...
        self.assertTrue(True)

