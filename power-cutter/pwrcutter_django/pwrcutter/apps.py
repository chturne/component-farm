from django.apps import AppConfig


class PwrcutterConfig(AppConfig):
    name = 'pwrcutter'
