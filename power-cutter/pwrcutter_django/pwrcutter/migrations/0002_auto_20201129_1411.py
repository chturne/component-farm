# Generated by Django 3.1.3 on 2020-11-29 14:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pwrcutter', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pdu',
            name='id',
        ),
        migrations.AlterField(
            model_name='pdu',
            name='hostname',
            field=models.TextField(primary_key=True, serialize=False),
        ),
    ]
