# gitlab-job-runner

Depends:
  - NFS
  - TFTP
  - Power cutter

Inputs:
  - ssh key material (not strictly needed on the private LAN)...

Actions:
  - Receives the job requests and dispatches to the farm LAN.
  - Unpacks a new rootfs and kernel, if necessary
  - Turns on power for the board and starts the test
  - Collects results

To build it and push to the docker registry,

```
docker build \
    -t registry.freedesktop.org/mupuf/valve-infra/gitlab-job-runner
    farm-runner/
docker push registry.freedesktop.org/mupuf/valve-infra/gitlab-job-runner
```

It is run by the `gitlab-runner` on per-job basis, not explicitly by
the valve-infra.

# Gitlab Runner
Depends:
  - gitlab-job-runner

Inputs:
  - GitLab config.toml

Principally no inputs and all registration is done behind the scenes.

Actions:
  - Start the runnner, which will wait for jobs from a GitLab instance
  - Register a new runner (i.e., a new board)

Start the runner,
Note that there's a bug running this in `linux/arm/v7` https://gitlab.com/gitlab-org/gitlab-runner/-/issues/25951
```
docker run --rm --name fdo-gitlab-runner \
 --mount type=bind,source=$(pwd)/gitlab-runner,target=/etc/gitlab-runner \
 --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
 --network=host gitlab/gitlab-runner:alpine-v13.5.0
```

Append a new board runner,
```
INSTANCE_URL='https://gitlab.freedesktop.org/'
REG_TOKEN=noxdxBkW_V5TXoEm8DkG
BOARD_NAME=igalia-amd-gfx8-2
TAG_LIST=igalia-amd-gfx8
docker run --rm \
  --mount type=bind,source=$(pwd)/gitlab-runner,target=/etc/gitlab-runner \
  --network=host \
  gitlab/gitlab-runner:alpine-v13.5.0 register \
  --non-interactive \
  --executor "docker" \
  --docker-image chturne/farm-runner \
  --url "$INSTANCE_URL" \
  --registration-token "$REG_TOKEN" \
  --description "$BOARD_NAME" \
  --tag-list "$TAG_LIST" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected" \
  --limit 1
  --docker-network-mode host
```

Note! The gitlab registration command gives no way to modify global
parameters. This is a big problem for automation, since the global
concurrency value very much depends on the number of runners! Hence,
we need to keep track of the number of runners we have installed, and
do a `sed` or something to change this if the registration is
successful.

Instead, we set a large `concurrent` value so that new runners will
always be able to pick up work, presumably the runner will queue up
the unmet slack for us nicely.

After configuration changes, you can manually restart the runner (but it should be picked up automatically,
```
docker restart gitlab-runner
```


