FROM python:3.8-alpine

RUN set -ex \
        && apk add --no-cache net-snmp-tools net-snmp-libs
COPY ./power-cutter /app
RUN set -ex \
        && apk add --no-cache --virtual .build-deps gcc libc-dev python3-dev net-snmp-dev \
        && ( cd /app ; pip install --no-cache-dir -r requirements.txt ) \
        && apk del --no-network .build-deps
WORKDIR /app/pwrcutter_django
# Clear any database from a previous development session, and recreate
# the database from migrations on fixture loads.
RUN rm -f db.sqlite3 && python manage.py makemigrations && python manage.py migrate
RUN python manage.py loaddata static-hosts-fixture.json
ARG DNS_SERVER=10.0.0.6
RUN echo ${DNS_SERVER} >> /resolv.conf
ENV PWRCUTTER_PORT=8089
EXPOSE $PWRCUTTER_PORT
# We need the shell here for variable interpolation
CMD ["sh", "-c", "python manage.py runserver 0.0.0.0:$PWRCUTTER_PORT"]
